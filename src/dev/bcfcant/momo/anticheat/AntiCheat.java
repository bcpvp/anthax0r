package dev.bcfcant.momo.anticheat;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import dev.bcfcant.momo.anticheat.types.RegenDetector;
import dev.bcfcant.momo.anticheat.types.SpeedDetector;

public class AntiCheat extends JavaPlugin {

	public static AntiCheat intance;

	public boolean isDebug = false;

	private Detectors detectors;

	public void onEnable() {
		intance = this;

		saveDefaultConfig();
		isDebug = getConfig().getBoolean("debug", false);

		Log.init(this);

		detectors = new Detectors(this);

		getDetectors().addDetector(new SpeedDetector(this));
		getDetectors().addDetector(new RegenDetector(this));

		// I temp removed anti knockback as it was bugged. I will re-add it later.
	}

	public Detectors getDetectors() {
		return detectors;
	}

	/**
	 * Logs a player getting caught
	 * 
	 * @param player The player who's hacking
	 * @param detector The detector that caught the player (hack type)
	 */
	public static void log(Player player, Detector detector) {
		log(player, detector, false);
	}

	private static HashMap<UUID, Pair<Detector, Long>> record = new HashMap<UUID, Pair<Detector, Long>>();

	public static void log(Player player, Detector detector, boolean kick) {
		if (record.containsKey(player.getUniqueId())) {
			Pair<Detector, Long> pair = record.get(player.getUniqueId());
			if (pair.getA().equals(detector)) {
				if (!Util.elapsed(pair.getB(), 1000)) {
					kick = true;
				}
			}
			record.remove(player.getUniqueId());
		}

		record.put(player.getUniqueId(), new Pair<Detector, Long>(detector, System.currentTimeMillis()));
		Log.info(player.getName() + " failed detector " + detector.getName() + ".");

		if (kick) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				if (p.hasPermission("anticheat.admin"))
					p.sendMessage(ChatColor.BLUE + "[antHax0r] " + ChatColor.RED + player.getName()
							+ " failed detector " + detector.getName() + " and has been kicked.");
			}

			player.kickPlayer(ChatColor.BLUE + "You were kicked for " + detector.getName() + " suspicions.\n"
					+ ChatColor.RED + "If you believe this is an error, please report it on our forums.\n\n"
					+ ChatColor.DARK_RED + "Continue cheating, and you will eventually get permanently banned!");
			record.remove(player.getUniqueId());
		}
	}
}
