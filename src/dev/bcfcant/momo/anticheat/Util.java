package dev.bcfcant.momo.anticheat;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import com.comphenix.protocol.injector.BukkitUnwrapper;
import com.comphenix.protocol.reflect.accessors.Accessors;
import com.comphenix.protocol.reflect.accessors.FieldAccessor;
import com.comphenix.protocol.utility.MinecraftReflection;

public class Util {

	private static FieldAccessor ON_GROUND = null;

	static {
		ON_GROUND = Accessors.getFieldAccessor(MinecraftReflection.getEntityClass(), "onGround", true);
	}

	public static double offset2d(Location a, Location b) {
		return offset2d(a.toVector(), b.toVector());
	}

	public static double offset2d(Vector a, Vector b) {
		a.setY(0);
		b.setY(0);
		return a.subtract(b).length();
	}

	public static double offset(Location a, Location b) {
		return offset(a.toVector(), b.toVector());
	}

	public static double offset(Vector a, Vector b) {
		return a.subtract(b).length();
	}

	public static boolean isGrounded(Entity ent) {
		try {
			Object nmsEntity = BukkitUnwrapper.getInstance().unwrapItem(ent);
			boolean onGround = (Boolean) ON_GROUND.get(nmsEntity);
			return onGround;
		} catch (Exception e) {
		}
		return solid(ent.getLocation().getBlock().getRelative(BlockFace.DOWN));
	}

	public static boolean solid(Block block) {
		return block != null && block.getType().isSolid();
	}

	public static boolean elapsed(long from, long required) {
		return System.currentTimeMillis() - from > required;
	}
}
