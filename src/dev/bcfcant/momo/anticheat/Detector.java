package dev.bcfcant.momo.anticheat;

import org.bukkit.event.Listener;

public class Detector implements Listener {

	protected AntiCheat core;
	private String name;

	public Detector(AntiCheat core, String name) {
		this.core = core;
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
