package dev.bcfcant.momo.anticheat.types;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import dev.bcfcant.momo.anticheat.AntiCheat;
import dev.bcfcant.momo.anticheat.Detector;
import dev.bcfcant.momo.anticheat.Util;

public class SpeedDetector extends Detector {

	// Amount of ticks the player has been moving too fast.
	// If this amount reaches over 6 ticks in a maximum of
	// 150ms delay, the player is kicked.
	private HashMap<UUID, Map.Entry<Integer, Long>> speedTicks = new HashMap<UUID, Map.Entry<Integer, Long>>();

	public SpeedDetector(AntiCheat antiCheat) {
		super(antiCheat, "Speed Hacking");
	}

	/*
	 * While this does work with fly hacking, it can be bypassed by the hacker by lowering the fly speed to 1 (creative
	 * fly).
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();

		if (player.getAllowFlight() || player.getGameMode() == GameMode.CREATIVE)
			return;

		int count = 0;

		if (this.speedTicks.containsKey(player.getUniqueId())) {
			double offset;
			if (event.getFrom().getY() > event.getTo().getY()) {
				offset = Util.offset2d(event.getFrom(), event.getTo());
			} else {
				offset = Util.offset(event.getFrom(), event.getTo());
			}

			double limit = 0.74D; // Fly limit
			if (Util.isGrounded(player)) {
				limit = 0.32D; // Ground limit
			}

			for (PotionEffect effect : player.getActivePotionEffects()) {

				// Speed effect should not be triggering the detector.
				if (effect.getType().equals(PotionEffectType.SPEED)) {
					if (Util.isGrounded(player)) {
						limit += 0.08D * (effect.getAmplifier() + 1);
					} else {
						limit += 0.04D * (effect.getAmplifier() + 1);
					}
				}
			}

			if ((offset > limit)
					&& (!Util.elapsed(((Long) ((Map.Entry<Integer, Long>) this.speedTicks.get(player.getUniqueId())).getValue()).longValue(), 150L))) {
				count = ((Integer) ((Map.Entry<Integer, Long>) this.speedTicks.get(player.getUniqueId())).getKey()).intValue() + 1;
			} else {
				count = 0;
			}

		}

		if (count > 6) {
			AntiCheat.log(player, this, false);
			event.setCancelled(true);
			count = 0; // Reset, since the player do it again.
		}

		// Save the ticks.
		this.speedTicks.put(player.getUniqueId(), new AbstractMap.SimpleEntry<Integer, Long>(Integer.valueOf(count), Long.valueOf(System.currentTimeMillis())));
	}

	@EventHandler
	public void onPlayerDisconnect(PlayerQuitEvent event) {
		handleDisconnect(event.getPlayer());
	}

	@EventHandler
	public void onPlayerDisconnect(PlayerKickEvent event) {
		handleDisconnect(event.getPlayer());
	}

	private void handleDisconnect(Player player) {
		this.speedTicks.remove(player.getUniqueId());
	}
}
