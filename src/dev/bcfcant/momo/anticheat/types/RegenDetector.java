package dev.bcfcant.momo.anticheat.types;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import dev.bcfcant.momo.anticheat.AntiCheat;
import dev.bcfcant.momo.anticheat.Detector;

public class RegenDetector extends Detector {

	// The last time the player regenerated, in milliseconds..
	private HashMap<UUID, Long> lastRegen = new HashMap<UUID, Long>();

	public RegenDetector(AntiCheat antiCheat) {
		super(antiCheat, "Regen Hacking");
	}

	@EventHandler
	public void onRegen(EntityRegainHealthEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;

		// Regeneration effect should not be triggering the detector.
		if (event.getRegainReason() != EntityRegainHealthEvent.RegainReason.SATIATED)
			return;

		Player player = (Player) event.getEntity();

		long currentTime = System.currentTimeMillis();

		if (!this.lastRegen.containsKey(player.getUniqueId())) {
			this.lastRegen.put(player.getUniqueId(), currentTime);
			return;
		}

		long last = this.lastRegen.get(player.getUniqueId());

		this.lastRegen.remove(player.getUniqueId());
		this.lastRegen.put(player.getUniqueId(), currentTime);

		/*
		 * Vanilla satiation regen is triggered every 5000 milliseconds, but we lower the limit to 3500 in case there
		 * were lag issues.
		 * 
		 * Usually, Hacking satiation regen is triggered below 500 milliseconds.
		 */
		if (currentTime - last < 3500) {
			event.setCancelled(true);
			AntiCheat.log(player, this);
		}
	}

	@EventHandler
	public void onPlayerDisconnect(PlayerQuitEvent event) {
		handleDisconnect(event.getPlayer());
	}

	@EventHandler
	public void onPlayerDisconnect(PlayerKickEvent event) {
		handleDisconnect(event.getPlayer());
	}

	private void handleDisconnect(Player player) {
		this.lastRegen.remove(player.getUniqueId());
	}
}
