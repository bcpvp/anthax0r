package dev.bcfcant.momo.anticheat;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

	private static AntiCheat CORE;
	private static Logger _log;

	public static void init(AntiCheat plugin) {
		CORE = plugin;
		_log = CORE.getLogger();
	}

	private static Logger log() {
		if (_log == null) {
			if (CORE == null)
				throw new IllegalStateException("The logger has not been initalised");
			_log = CORE.getLogger();
		}
		return _log;
	}

	public static Logger getLogger() {
		return log();
	}

	public static void log(Level level, Object object) {
		log().log(level, String.valueOf(object));
	}

	public static void log(Level level, Object object, Throwable ex) {
		log().log(level, String.valueOf(object), ex);
	}

	public static void fine(Object object) {
		log(Level.FINE, object);
	}

	public static void finer(Object object) {
		log(Level.FINER, object);
	}

	public static void finest(Object object) {
		log(Level.FINEST, object);
	}

	public static void info(Object object) {
		log(Level.INFO, object);
	}

	public static void severe(Object object) {
		log(Level.SEVERE, object);
	}

	public static void severe(Object object, Throwable ex) {
		log(Level.SEVERE, object);
		ex.printStackTrace();
	}

	public static void warning(Object object) {
		log(Level.WARNING, object);
	}

	public static void warning(Object object, Throwable ex) {
		log(Level.WARNING, object);
		ex.printStackTrace();
	}

	public static void debug(Object object) {
		if (CORE.isDebug)
			log().info("[DEBUG] " + String.valueOf(object));
	}
}
