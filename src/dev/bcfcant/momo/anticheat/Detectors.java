package dev.bcfcant.momo.anticheat;

import java.util.LinkedHashSet;
import java.util.Set;

public class Detectors {

	private AntiCheat core;
	private Set<Detector> detectors = new LinkedHashSet<Detector>();

	public Detectors(AntiCheat core) {
		this.core = core;
	}

	public void addDetector(Detector detector) {
		detectors.add(detector);
		core.getServer().getPluginManager().registerEvents(detector, core);
		Log.debug("Added detector: " + detector.getName());
	}

	public Set<Detector> getDetectors() {
		return detectors;
	}
}
